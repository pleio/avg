FROM python:3.9-slim AS build

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential

RUN python -m venv /app/venv && /app/venv/bin/pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app
RUN /app/venv/bin/pip3 install -r requirements.txt

FROM python:3.9-slim
RUN apt-get update && apt-get install --no-install-recommends -y \
    mime-support

COPY --from=build /app/venv /app/venv
ENV PATH="/app/venv/bin:${PATH}"

WORKDIR /app
COPY . /app

COPY ./docker/config.py /app/avg/config.py

COPY ./docker/start.sh /
RUN chmod +x /start.sh

RUN mkdir -p /app/static && chown www-data:www-data /app/static

ENV PYTHONUNBUFFERED 1

# HTTP port
EXPOSE 8000

# Define run script
CMD ["/start.sh"]
USER www-data
