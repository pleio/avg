from django.db import models
from pleio_auth.models import User

class Subsite(models.Model):
    name = models.CharField(max_length=1000)

    def __str__(self):
        return self.name

class SignedAgreement(models.Model):
    subsite = models.ManyToManyField(Subsite, related_name='signed_agreement')
    in_name_of_processing_terms = models.CharField(max_length=150, blank=True)
    accepted_processing_terms = models.BooleanField(default=False, blank=True)
    in_name_of_user_terms = models.CharField(max_length=150, blank=True)
    accepted_user_terms = models.BooleanField(default=False, blank=True)
    version = models.CharField(max_length=10)
    organisation = models.CharField(max_length=150)
    signed_by = models.ForeignKey(User, related_name='signed_agreements', on_delete=models.CASCADE)
    signed_on = models.DateTimeField(auto_now_add=True)
