from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect, render
from django.contrib import messages
from .models import Subsite, SignedAgreement
from .forms import SubsiteForm, SignedAgreementForm

@login_required
def add_subsite(request):
    if request.POST:
        form = SubsiteForm(request.POST)
        if form.is_valid():
            subsite = form.save(commit=False)
            subsite.save()
            messages.add_message(request, messages.INFO, 'De subsite {} is toegevoegd.'.format(subsite.name))

            return redirect('/subsites/add')
    else:
        form = SubsiteForm()

    return render(request, 'add_subsite.html', {
        'form': form
    })

@login_required
def index(request):
    if request.POST:
        form = SignedAgreementForm(request.POST)
        form.fields.pop('signed_by')
        if form.is_valid():
            signed_agreement = form.save(commit=False)
            signed_agreement.signed_by = request.user
            signed_agreement.version = '2.0'
            signed_agreement.save()
            form.save_m2m()
            messages.add_message(request, messages.INFO, 'De overeenkomst is ondertekend.')

            return redirect('/')
    else:
        form = SignedAgreementForm(initial={'signed_by': request.user.name})

    return render(request, 'index.html', {
        'form': form,
        'user_signed_agreements': SignedAgreement.objects.filter(signed_by=request.user),
        'user': request.user
    })

@login_required
def terms(request, terms_type):
    if terms_type in ['user', 'processing']:
        return render(request, 'terms/{}.html'.format(terms_type))
    raise Http404('Could not find this type of terms.')
