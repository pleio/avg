# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-05-23 18:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20180523_1854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signedagreement',
            name='in_name_of_processing_terms',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AlterField(
            model_name='signedagreement',
            name='in_name_of_user_terms',
            field=models.CharField(blank=True, max_length=150),
        ),
    ]
