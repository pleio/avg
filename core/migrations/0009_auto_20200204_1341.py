# Generated by Django 3.0.3 on 2020-02-04 12:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20180523_2027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signedagreement',
            name='accepted_processing_terms',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='signedagreement',
            name='accepted_user_terms',
            field=models.BooleanField(blank=True, default=False),
        ),
    ]
