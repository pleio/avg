from django.contrib import admin
from .models import Subsite, SignedAgreement

class SubsiteAdmin(admin.ModelAdmin):
    def has_processing_agreement(self, obj):
        if len(obj.signed_agreement.filter(accepted_processing_terms=True)) > 0:
            return True
        return False
    def has_user_agreement(self, obj):
        if len(obj.signed_agreement.filter(accepted_user_terms=True)) > 0:
            return True
        return False
    has_processing_agreement.boolean = True
    has_user_agreement.boolean = True

    list_display = ('name', 'has_processing_agreement', 'has_user_agreement')

class SignedAgreementAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def subsites(self, obj):
        subsites = "| "
        for subsite in obj.subsite.all():
            s = " " + subsite.name + " |"
            subsites += s
        return subsites

    def signed_by_email(self, obj):
        return obj.signed_by.email

    readonly_fields = ['subsite', 'organisation', 'signed_by', 'signed_on',
                       'in_name_of_processing_terms', 'accepted_processing_terms',
                       'in_name_of_user_terms', 'accepted_user_terms', 'version']
    list_display = ('subsites', 'organisation', 'in_name_of_processing_terms',
                    'in_name_of_user_terms', 'version', 'signed_by_email', 'signed_by', 'signed_on')

admin.site.register(SignedAgreement, SignedAgreementAdmin)
admin.site.register(Subsite, SubsiteAdmin)
