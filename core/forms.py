from django.forms import ModelForm, ModelChoiceField, ModelMultipleChoiceField, BooleanField, CheckboxInput, TextInput, SelectMultiple, ValidationError
from .models import SignedAgreement, Subsite

class SubsiteForm(ModelForm):
    class Meta:
        model = Subsite
        fields = ['name', ]

        labels = {
            'name': 'Naam subsite* (link)',
        }

        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
        }

class SignedAgreementForm(ModelForm):

    class Meta:
        model = SignedAgreement
        fields = ['subsite', 'signed_by', 'in_name_of_processing_terms', 'accepted_processing_terms',
                  'in_name_of_user_terms', 'accepted_user_terms', 'organisation']

        labels = {
            'organisation': 'Organisatie*',
            'in_name_of_processing_terms': 'Akkoord namens verantwoordelijke Functionaris Gegevensbescherming (vul naam in)*',
            'in_name_of_user_terms': 'Akkoord namens verantwoordelijke manager/bestuurder (vul naam in)*',
            'signed_by': 'Naam: ',
        }

        widgets = {
            'organisation': TextInput(attrs={'class': 'form-control'}),
            'in_name_of_processing_terms': TextInput(attrs={'class': 'form-control'}),
            'in_name_of_user_terms': TextInput(attrs={'class': 'form-control'}),
            'signed_by': TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        }

    subsite = ModelMultipleChoiceField(
        required=True,
        queryset=Subsite.objects.all().exclude(signed_agreement__accepted_processing_terms=True, signed_agreement__accepted_user_terms=True).order_by('name'),
        label='Deelsite* (houd CTRL ingedrukt om meerdere te selecteren)',
        widget=SelectMultiple(attrs={'class': 'form-control', 'size': 10})
    )

    accepted_processing_terms = BooleanField(
        required=False,
        widget=CheckboxInput(attrs={'class': 'form-check-input'}),
        label='Ja, ik accepteer de verwerkersovereenkomst'
    )

    accepted_user_terms = BooleanField(
        required=False,
        widget=CheckboxInput(attrs={'class': 'form-check-input'}),
        label='Ja, ik accepteer de gebruikersovereenkomst'
    )

    def clean(self):
        cleaned_data = super(SignedAgreementForm, self).clean()

        in_name_of_processing_terms = cleaned_data.get("in_name_of_processing_terms")
        accepted_processing_terms = cleaned_data.get("accepted_processing_terms")
        in_name_of_user_terms = cleaned_data.get("in_name_of_user_terms")
        accepted_user_terms = cleaned_data.get("accepted_user_terms")

        if not accepted_processing_terms and not accepted_user_terms:
            msg = "Accepteer Verwerkersovereenkomst en/of Gebruikersovereenkomst"
            self.add_error('accepted_processing_terms', msg)
            self.add_error('accepted_user_terms', msg)

        if accepted_processing_terms and in_name_of_processing_terms.strip() == "":
            msg = "Wanneer Verwerkersovereenkomst is geaccepteerd, dan is naam Functionaris Gegevensbescherming verplicht."
            self.add_error('in_name_of_processing_terms', msg)
        if accepted_user_terms and in_name_of_user_terms.strip() == "":
            msg = "Wanneer Gebruikersovereenkomst is geaccepteerd, dan is naam manager/bestuurder verplicht."
            self.add_error('in_name_of_user_terms', msg)
