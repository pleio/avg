from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings
from django.core.mail import send_mail
import urllib.parse

class AccessRequest(models.Model):
    external_id = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Manager(BaseUserManager):
    def create_user(self, email, name, password=None, external_id=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            name=name
        )

        user.is_active = True

        if password:
            user.set_password(password)
        
        if external_id:
            user.external_id = external_id

        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        user = self.create_user(
            email=self.normalize_email(email),
            name=name,
            password=password
        )

        user.is_admin = True
        user.is_active = True

        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    objects = Manager()

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=255, unique=True)
    external_id = models.CharField(max_length=255, unique=True)

    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    REQUIRED_FIELDS = ['name']
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # not implemented
        return True

    def has_module_perms(self, app_label):
        # not implemented
        return True

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def icon_url(self):
        if not self.external_id:
            return None

        return urllib.parse.urljoin(settings.PLEIO_OAUTH_URL, "mod/profile/icondirect.php?guid={}&size=large".format(self.external_id))

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @property
    def is_staff(self):
        return self.is_admin

    def __str__(self):
        return self.name