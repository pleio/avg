from django.shortcuts import render, redirect
from oauthlib.oauth2.rfc6749.errors import MismatchingStateError, InvalidGrantError
from django.utils.crypto import get_random_string
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from .models import User, AccessRequest
from django.contrib.auth import logout as auth_logout, login as auth_login
from django.db import IntegrityError
from django.core import signing
from django.conf import settings
from django.urls import reverse
from requests_oauthlib import OAuth2Session
from django.template.loader import render_to_string
from django.core.mail import send_mail
from .lib import get_admin_emails
import urllib.parse

def login(request):
    oauth = OAuth2Session(settings.PLEIO_OAUTH_CLIENT_ID,
        redirect_uri=request.build_absolute_uri(reverse('login_response')),
        state=get_random_string(12)
    )

    authorization_url, state = oauth.authorization_url(urllib.parse.urljoin(settings.PLEIO_OAUTH_URL, 'oauth/v2/authorize'))
    request.session['oauth_state'] = state

    return redirect(authorization_url)

def login_response(request):
    oauth = OAuth2Session(settings.PLEIO_OAUTH_CLIENT_ID,
        redirect_uri=request.build_absolute_uri(reverse('login_response')),
        state=request.session.get('oauth_state')
    )

    try:
        token = oauth.fetch_token(urllib.parse.urljoin(settings.PLEIO_OAUTH_URL, 'oauth/v2/token'),
            client_secret=settings.PLEIO_OAUTH_CLIENT_SECRET,
            authorization_response=request.build_absolute_uri(reverse('login_response')) + '?' + request.GET.urlencode()
        )
    except (MismatchingStateError, InvalidGrantError):
        return redirect('retry')

    profile = oauth.get(urllib.parse.urljoin(settings.PLEIO_OAUTH_URL, 'api/users/me')).json()
    try:
        user = User.objects.get(external_id=profile['guid'])
        if user.is_active:
            auth_login(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            return render(request, 'account_blocked.html', {
                'profile': profile
            })

    except ObjectDoesNotExist:
        if settings.PLEIO_OAUTH_REQUEST_ACCESS:
            return render(request, 'request_access.html', {
                'signature': signing.dumps(profile),
                'profile': profile
            })
        else:
            user = User.objects.create_user(
                email = profile['email'],
                name = profile['name'],
                password = None,
                external_id = profile['guid']
            )

            auth_login(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)

def request_access(request):
    try:
        profile = signing.loads(request.POST.get('signature'))
    except signing.BadSignature:
        return render(request, 'retry.html')

    access_request, created = AccessRequest.objects.get_or_create(
        email=profile['email'],
        name=profile['name'],
        external_id=profile['guid']
    )

    send_mail(
        render_to_string('emails/access_request_subject.txt', { 'profile': profile, 'host': request.get_host() }),
        render_to_string('emails/access_request.txt', { 'profile': profile, 'host': request.get_host(), 'url': request.build_absolute_uri(reverse('admin_login')) }),
        settings.EMAIL_FROM,
        get_admin_emails(),
        fail_silently=True
    )

    if not created:
        access_request.created_at = timezone.now()
        access_request.save()

    return render(request, 'access_requested.html')

def retry(request):
    return render(request, 'retry.html')

def logout(request):
    auth_logout(request)
    return redirect(urllib.parse.urljoin(settings.PLEIO_OAUTH_URL, 'action/logout'))
