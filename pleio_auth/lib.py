from .models import User
from email.utils import formataddr

def get_admin_emails():
    admins = User.objects.filter(is_admin=True)
    return [formataddr((admin.name, admin.email)) for admin in admins]