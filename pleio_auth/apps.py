from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'pleio_auth'
