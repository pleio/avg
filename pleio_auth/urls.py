from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^login_response/$', views.login_response, name='login_response'),
    url(r'^request_access/$', views.request_access, name='request_access'),
    url(r'^retry/$', views.retry, name='retry'),
    url(r'^logout/$', views.logout, name='logout')
]
