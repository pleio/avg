Beste beheerder,

De gebruiker {{ profile.name }} met het e-mailadres {{ profile.email }} heeft toegang aangevraagd tot {{host}}. Ga naar het beheerpaneel om de toegang te verlenen:

{{url}}

Groeten,

Flow.