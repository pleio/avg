#!/bin/bash

# Collect static
python manage.py collectstatic --noinput

# Run migrations
python manage.py migrate

# Start Gunicorn processes
echo Starting uwsgi
uwsgi --http :8000 --module avg.wsgi --static-map /static=/app/static --static-map /media=/app/media
