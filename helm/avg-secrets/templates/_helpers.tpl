{{- define "avg.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "avg.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "avg.labels" -}}
helm.sh/chart: {{ include "avg.chart" . }}
{{ include "avg.selectorLabels" . }}
{{- end -}}

{{- define "avg.selectorLabels" -}}
app.kubernetes.io/name: {{ include "avg.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
