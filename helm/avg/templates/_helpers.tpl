{{/*
Expand the name of the chart.
*/}}
{{- define "avg.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "avg.secretsName" -}}
{{- printf "%s-secrets" ((include "avg.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "avg.tlsSecretName" -}}
{{- printf "tls-wildcard.%s.nl" ( split "." (.Values.domain ))._1 }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "avg.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "avg.labels" -}}
helm.sh/chart: {{ include "avg.chart" . }}
{{ include "avg.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "avg.selectorLabels" -}}
app.kubernetes.io/name: {{ include "avg.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
